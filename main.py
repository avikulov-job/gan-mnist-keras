#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from os import path as ospath

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from keras.layers import Input
from keras.models import Model, Sequential
from keras.layers.core import Dense, Dropout
from keras.layers.advanced_activations import LeakyReLU
from keras.datasets import mnist
from keras.optimizers import Adam
from keras import initializers

# Let Keras know that we are using tensorflow as our backend engine.
# os.environ['KERAS_BACKEND'] = 'tensorflow'

# To make sure that we can reproduce the
# experiment and get the same results.
np.random.seed(10)

MAIN_DIR = ospath.dirname(__file__)
DATA_DIR = ospath.join(MAIN_DIR, 'data')

IMAGES_DIR = ospath.join(DATA_DIR, 'images')
DATASETS_DIR = ospath.join(DATA_DIR, 'datasets')
MNIST_DATASET_DIR = ospath.join(DATASETS_DIR, 'mnist')

IMG_WIDTH = 28
IMG_HEIGHT = IMG_WIDTH
# 28 x 28 = 784
ROW_LEN = (IMG_HEIGHT * IMG_WIDTH)

# The dimension of our random noise vector.
RANDOM_DIM = 100


def create_subdirs():
    for subdir in (DATA_DIR, IMAGES_DIR, DATASETS_DIR, MNIST_DATASET_DIR):
        if ospath.exists(subdir) and ospath.isdir(subdir):
            continue

        try:
            os.mkdir(subdir, mode=0o777)
        except Exception as e:
            print(e)
            continue

    return None


def update_images_size(width=28, height=28):
    global IMG_WIDTH, IMG_HEIGHT, ROW_LEN

    IMG_HEIGHT, IMG_WIDTH = height, width
    ROW_LEN = (IMG_HEIGHT * IMG_WIDTH)

    return None


def load_mnist_data():
    # load the data
    # data_path = ospath.join(MNIST_DATASET_DIR, 'mnist.npz')
    # relative to ~/.keras/datasets
    data_path = 'mnist.npz'
    (x_train, y_train), (x_test, y_test) = mnist.load_data(data_path)

    # normalize our inputs to be in the range [-1, 1]
    x_train = ((x_train.astype(np.float32) - 127.5) / 127.5)

    # convert x_train with a shape of (60000, 28, 28) to
    # (60000, 784) so we have 784 columns per row.
    count, height, width = x_train.shape
    update_images_size(width=width, height=height)

    x_train = x_train.reshape(count, ROW_LEN)
    return (x_train, y_train, x_test, y_test)


def get_optimizer():
    return Adam(lr=0.0002, beta_1=0.5)


def get_generator(optimizer):
    generator = Sequential()

    generator.add(
        Dense(256,
              input_dim=RANDOM_DIM,
              kernel_initializer=initializers.RandomNormal(stddev=0.02)))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(512))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(1024))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(ROW_LEN, activation='tanh'))
    generator.compile(loss='binary_crossentropy',
                      optimizer=optimizer)

    return generator


def get_discriminator(optimizer):
    discriminator = Sequential()

    discriminator.add(
        Dense(1024,
              input_dim=ROW_LEN,
              kernel_initializer=initializers.RandomNormal(stddev=0.2)))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(512))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(256))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(1, activation='sigmoid'))
    discriminator.compile(loss='binary_crossentropy',
                          optimizer=optimizer)

    return discriminator


def get_gan_network(discriminator, generator, optimizer):
    # We initially set trainable to False since we only want
    # to train either the generator or discriminator at a time
    discriminator.trainable = False

    # gan input (noise) will be 100-dimensional vectors.
    gan_input = Input(shape=(RANDOM_DIM,))

    # the output of the generator (an image).
    x = generator(gan_input)

    # get the output of the discriminator
    # (probability if the image is real or not).
    gan_output = discriminator(x)

    gan = Model(inputs=gan_input, outputs=gan_output)
    gan.compile(loss='binary_crossentropy',
                optimizer=optimizer)

    return gan


def plot_generated_images(epoch, generator, examples=100,
                          dim=(10, 10), figsize=(10, 10)):

    noise = np.random.normal(0, 1, size=[examples, RANDOM_DIM])
    generated_images = generator.predict(noise)
    generated_images = generated_images.reshape(
        examples, IMG_HEIGHT, IMG_WIDTH)

    plt.figure(figsize=figsize)
    for i in range(examples):
        plt.subplot(dim[0], dim[1], (i+1))
        plt.imshow(
            generated_images[i], interpolation='nearest', cmap='gray_r')
        plt.axis('off')

    img_path = ospath.join(
        IMAGES_DIR, f'gan_generated_image_epoch_{epoch:0>5}.png')

    plt.tight_layout()
    plt.savefig(img_path)

    return None


def train(epochs=1, batch_size=128):
    # get the training and testing data
    x_train, y_train, x_test, y_test = load_mnist_data()

    # split the training data into batches of size 128
    batch_count = int(x_train.shape[0] / batch_size)

    # build our gan network
    optimizer = get_optimizer()
    generator = get_generator(optimizer)
    discriminator = get_discriminator(optimizer)
    gan = get_gan_network(discriminator, generator, optimizer)

    # summary
    print('\n')
    for name, model in [('GENERATOR', generator),
                        ('DISCRIMINATOR', discriminator),
                        ('GAN', gan)]:
        print(name)
        model.summary()
        print('\n')

    div_str = ('-' * 15)
    for e in range(1, (epochs+1)):
        print(f'{div_str}\nEpoch {e:0>5}\n{div_str}')

        for bc in tqdm(range(batch_count)):
            # get a random set of input noise and images
            noise = np.random.normal(0, 1, size=[batch_size, RANDOM_DIM])
            image_batch = x_train[
                np.random.randint(0, x_train.shape[0], size=batch_size)]

            # generate fake mnist images
            generated_images = generator.predict(noise)
            X = np.concatenate([image_batch, generated_images])

            # label for generated and real data
            y_dis = np.zeros(2 * batch_size)
            # one-sided label smoothing
            y_dis[:batch_size] = 0.9

            # train discriminator
            discriminator.trainable = True
            discriminator.train_on_batch(X, y_dis)

            # train generator
            noise = np.random.normal(0, 1, size=[batch_size, RANDOM_DIM])
            y_gen = np.ones(batch_size)
            discriminator.trainable = False
            gan.train_on_batch(noise, y_gen)

        if (e % 20) == 0:
            plot_generated_images(e, generator)


def main():
    create_subdirs()
    train(1, 128)
    return None


if __name__ == '__main__':
    main()
